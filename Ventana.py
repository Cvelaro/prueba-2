import tkinter
from tkinter import *
import funcion

# --Inicio ventana--
ventana = Tk()
ventana.title("Lista Halloween")
vp = Frame(ventana)
vp.grid(column = 0, row = 0, padx = (80,80), pady = (12,12)) # Establece la distancia del limite de la ventana con vp
vp.columnconfigure(0, weight = 1)
vp.rowconfigure(0, weight = 1)
ventana.resizable(0, 0) # Elimina el boton de maximizar la ventana

# --Creamos desplegable--
var = tkinter.StringVar(vp)
var.set('Añadir')
opciones = ['Añadir', 'Quitar']
opcion = tkinter.OptionMenu(vp, var, *opciones)
opcion.config(width = 20)
opcion.grid(column = 1, row = 0)

# --Creamos etiqueta--
etiqueta = tkinter.Label(vp, text = "Nombre")
etiqueta.grid(column = 0, row = 1)

# --Creamos caja de texto--
cajaTexto = tkinter.Entry(vp, font = "Helvetica 10")
cajaTexto.grid(column = 1, row = 1, pady = (10, 10))

# --Funcion del boton--
li = []
def acceder():
    nombre = cajaTexto.get()
    if var == 'Añadir':
        li.append(nombre)
        print(li)
    else:
        li.remove(nombre)

# --Boton--
boton = tkinter.Button(vp, text = 'Confirmar', command = acceder)
boton.grid(column = 1, row = 2)

# --Fin de la ventana--
ventana.mainloop()


